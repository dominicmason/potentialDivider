import pandas as pd
import numpy as np
import itertools

rThermistor = 1328

def getResistance(code):
    exps = {"R": "e0", "K": "e3", "M": "e6"}
    exp = "R"
    decode = ""
    for c in code:
        if c in exps.keys():
            exp = c
            decode += "."
        else:
            decode += c
    decode += exps[exp]
    return float(decode)

def potDiv(r1, r2):
    return 5 * (r2/(r1+r2))

def isClear(voltage):
    return 5 - voltage < 4.8 and voltage > 0.2

class Combination:
    def __init__(self, comb):
        self.r1 = comb[0]
        self.r2 = comb[1]
        self.r3 = comb[2]
        self.ref = potDiv(self.r1, self.r2)
        self.out = potDiv(self.r3, rThermistor)
        self.dist = self.ref - self.out
    def isClear(self):
        return isClear(self.ref) and isClear(self.out)
    def isBelow(self):
        return self.dist > 0
    def describe(self):
        desc = f"\nr1: {self.r1}"
        desc += f"\nr2: {self.r2}"
        desc += f"\nr3: {self.r3}"
        desc += f"\nReference: {self.ref:.4}"
        desc += f"\nOutput at 85 C: {self.out:.4}"
        desc += f"\nDistance to Reference: {self.dist:.4f}"
        return desc

def getResList():
    resFile = pd.read_csv("resistors.csv")
    inStock = resFile.loc[resFile["Stock"] > 0]
    resList = []
    for index, res in inStock.iterrows():
        resList.append(getResistance(res["Description"].split(",")[1]))
    return resList

def getCombinations(resList):
    it = itertools.combinations(resList, 3)
    combs = []
    for combTuple in it:
        comb = Combination(combTuple)
        if comb.isClear() and comb.isBelow():
            combs.append(comb)        
    return combs

resList = getResList()
print(len(resList), "Resistors in stock")
combs = getCombinations(resList)
print(len(combs), "Combinations found")
best = min(combs, key = lambda c: c.dist)
print("Best found:", best.describe())
